
#-----Statement of Authorship----------------------------------------#
#
#  This is an individual assessment item.  By submitting this
#  code I agree that it represents my own work.  I am aware of
#  the University rule that a student must not act in a manner
#  which constitutes academic dishonesty as stated and explained
#  in QUT's Manual of Policies and Procedures, Section C/5.3
#  "Academic Integrity" and Section E/2.1 "Student Code of Conduct".
#
#    Student no: n8051381   
#    Student name: Min Ho Kim 
#
#  NB: Files submitted without a completed copy of this statement
#  will not be marked.  Submitted files will be subjected to
#  software plagiarism analysis using the MoSS system
#  (http://theory.stanford.edu/~aiken/moss/).
#
#--------------------------------------------------------------------#



#-----Assignment Description-----------------------------------------#
#
#  Online Shopper
#
#  In this assignment you will combine your knowledge of HTMl/XML
#  mark-up languages with your skills in Python scripting, pattern
#  matching, and Graphical User Interface design to produce a useful
#  application for aggregating product data published by a variety of
#  online shops.  See the instruction sheet accompanying this file
#  for full details.
#
#--------------------------------------------------------------------#



#-----Imported Functions---------------------------------------------#
#
# Below are various import statements for helpful functions.  You
# should be able to complete this assignment using these
# functions only.  Note that not all of these functions are
# needed to successfully complete this assignment.

# The function for opening a web document given its URL.
# (You WILL need to use this function in your solution.)
from urllib import urlopen

# Import the standard Tkinter functions. (You WILL need to use
# these functions in your solution.)
from Tkinter import *

# Functions for finding all occurrences of a pattern
# defined via a regular expression.  (You do NOT need to
# use these functions in your solution, although you will find
# it difficult to produce a robust solution without using
# regular expressions.)
from re import findall, finditer

# Import the standard SQLite functions just in case they're
# needed.
from sqlite3 import *

#
#--------------------------------------------------------------------#



#-----Student's Solution---------------------------------------------#
#
# Put your solution at the end of this file.
#

# Write HTML template
html_template = """
    <!DOCTYPE html>
    <html>
        <head>
            <title>Little River Trading Co. Invoice</title>
            <!-- Latest compiled and minified CSS -->
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        </head>
        <style>

            .invoice-head-row, .comp-logo-row, .comp-total-row{
                text-align: center;
            }
            .invoice-head-row{
                width: 100%;
                padding: 10px;
                float: left;
                background: #fdaf17;
                color: #fff;
                font-size: 30px;
                margin: 40px 0;
            }
            .comp-logo-row img{
                width: 100%;
            }
            .comp-item-row{
                margin: 40px 0;
            }
            .comp-item-row .row{
                margin: 5px 0;
            }
            .comp-item-row img{
                width: 100px;
                height: auto;
            }
            .comp-item-row .media-heading{
                font-size: 16px;
            }
        </style>
        <body>
            <div class="container">

                <div class="row invoice-head-row">
                    <div class="col-xs-12 col-sm-6 col-sm-offset-3">
                        <p>Invoice</p>
                    </div>
                </div>

                <div class="row comp-logo-row">
                    <div class="col-xs-12 col-sm-6 col-sm-offset-3">
                        <img src="https://friendsofthesmokies.org/app/uploads/2015/05/Little-River-Trading-Co.jpg">
                    </div>
                </div>

                <div class="row comp-total-row">
                    <div class="col-xs-12 col-sm-6 col-sm-offset-3">
                        <h3>Total for purchases below:<br/> $ ***TOTAL*** (AUD)</h3>
                    </div>
                </div>

                <div class="row comp-item-row">
                    <div class="col-xs-12 col-sm-6 col-sm-offset-3">
                        ***ITEM***
                    </div>
                </div>

                <div class="row comp-support-row">
                    <div class="col-xs-12">
                        <p>The Littile River Trading Company is proudly supported by:</p>
                        <ul>
                            <li><a href="https://www.zazzle.com/rss?qs=iphone+7+cases">https://www.zazzle.com/rss?qs=iphone+7+cases</a></li>
                            <li><a href="http://www.tigerdirect.com/xml/rsstigercat17.xml">http://www.tigerdirect.com/xml/rsstigercat17.xml</a></li>
                            <li><a href="https://www.crimezappers.com/rss/catalog/category/cid/97/store_id/1/">https://www.crimezappers.com/rss/catalog/category/cid/97/store_id/1/</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </body>
    </html>
"""

html_nocharge_template = """
    <!DOCTYPE html>
    <html>
        <head>
            <title>Little River Trading Co. Invoice</title>
            <!-- Latest compiled and minified CSS -->
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        </head>
        <style>
        
            .invoice-head-row, .comp-logo-row, .comp-total-row, .nocharge{
                text-align: center;
            }
            .invoice-head-row{
                width: 100%;
                padding: 10px;
                float: left;
                background: #fdaf17;
                color: #fff;
                font-size: 30px;
                margin: 40px 0;
            }
            .comp-logo-row img{
                width: 100%;
            }
            .comp-item-row{
                margin: 40px 0;
            }
            .comp-item-row .row{
                margin: 5px 0;
            }
            .comp-item-row img{
                width: 100px;
                height: auto;
            }
    
        </style>
        <body>
            <div class="container">

                <div class="row invoice-head-row">
                    <div class="col-xs-12 col-sm-6 col-sm-offset-3">
                        <p>Invoice</p>
                    </div>
                </div>

                <div class="row comp-logo-row">
                    <div class="col-xs-12 col-sm-6 col-sm-offset-3">
                        <img src="https://friendsofthesmokies.org/app/uploads/2015/05/Little-River-Trading-Co.jpg">
                    </div>
                </div>

                <div class="row comp-item-row nocharge">
                    <div class="col-xs-12 col-sm-6 col-sm-offset-3">
                        <h3>Thank you for browsing<br/>
                            Please call again</h3>
                        <p>You didn't choose any item.</p>
                    </div>
                </div>

                <div class="row comp-support-row">
                    <div class="col-xs-12">
                        <p>The Littile River Trading Company is proudly supported by:</p>
                        <ul>
                            <li><a href="https://www.zazzle.com/rss?qs=iphone+7+cases">https://www.zazzle.com/rss?qs=iphone+7+cases</a></li>
                            <li><a href="http://www.tigerdirect.com/xml/rsstigercat17.xml">http://www.tigerdirect.com/xml/rsstigercat17.xml</a></li>
                            <li><a href="https://www.crimezappers.com/rss/catalog/category/cid/97/store_id/1/">https://www.crimezappers.com/rss/catalog/category/cid/97/store_id/1/</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </body>
    </html>
"""

#====Functions====#
# Bind item name, price and image together
def bind_product_details(names, prices, images):
        # make a list to arrange item lists
        items = []
        # retrieve name, price and image one by one, and chunk them together
        i = 0
        for _ in range(5):
                tmp_list = []
                tmp_list.append(names[i])
                tmp_list.append(prices[i])
                tmp_list.append(images[i])
                items.append(tmp_list)
                i+=1
        # return arranged items
        return items

# This function is for aggregating each item's prices of a category
def calculate_price(items, quantity):
        total_price = 0
        for item_details in items[:int(quantity)]:
                total_price += float(item_details[1])
        return ("{0:.2f}".format(round(total_price,2)))

# This is the function for generating HTML code for displaying item section
def generate_item_html(items, quantity):
        item_code = ""
        for item_details in items[:int(quantity)]:
                item_code = str(item_code) + """
                        <div class="row">
                                <div class="col-xs-12">
                                        <div class="media">
                                                  <div class="media-left media-middle">
                                                        <a href="#">
                                                              <img class="media-object img-thumbnail" src='""" + item_details[2] + """'>
                                                        </a>
                                                  </div>
                                                  <div class="media-body">
                                                        <h4 class="media-heading">""" + item_details[0] + """</h4>
                                                        <p>$ """ + item_details[1] + """</p>
                                                  </div>
                                        </div>
                                </div>
                        </div>
                """
        return item_code

# This is the function that generates the HTML document
def generate_html(cate1, cate2, cate3, item_q):
        # Get the document's title
        title = "Little River Trading"
        # Quantities of each category
        iphone_case_q = item_q["iphone_case_quantity"]
        notebook_q = item_q["notebook_quantity"]
        hidden_cam_q = item_q["hidden_cam_quantity"]
        # Get total price for displaying on the invoice
        iphone_case_total_price = calculate_price(cate1, iphone_case_q)
        notebook_total_price = calculate_price(cate2, notebook_q)
        hidden_cam_total_price = calculate_price(cate3, hidden_cam_q)
        total_price = float(iphone_case_total_price) + float(notebook_total_price) + float(hidden_cam_total_price)
        # Replace the blanks in the HTML template
        html_code = html_template.replace('***TOTAL***',str(total_price))
        category_1_html = generate_item_html(cate1, iphone_case_q)
        category_2_html = generate_item_html(cate2, notebook_q)
        category_3_html = generate_item_html(cate3, hidden_cam_q)
       
        items = str(category_1_html) + str(category_2_html) + str(category_3_html)

        html_code = html_code.replace('***ITEM***', items)

        # Write the HTML code to a file
        html_file = open('invoice_' + title + '.html', 'w')
        html_file.write(html_code)
        html_file.close()

        # Update process status
        process.config(text='Done!')

# This is the function that genates the no charge HTML invoice
def generate_nocharge_html():
        # Get the document's title
        title = "Little River Trading"
        # Write the no charge HTML template to a file
        html_file = open('invoice_' + title + '.html', 'w')
        html_file.write(html_nocharge_template)
        html_file.close()
        
# Get the number of items of each category
def get_quantity():
        iphone_case_quantity = int(iPhone_spin.get())
        notebook_quantity = int(notebook_spin.get())
        hidden_cam_quantity = int(hiddenCameras_spin.get())

        # If nothing selected, create empty invoice
        if (iphone_case_quantity == 0) and (notebook_quantity == 0) and (hidden_cam_quantity == 0):
                return False
        else:
                item_quantities = {
                                        'iphone_case_quantity':iphone_case_quantity,
                                        'notebook_quantity':notebook_quantity,
                                        'hidden_cam_quantity':hidden_cam_quantity
                                        }
                return item_quantities
        

# Get the number of items of each category and run generate html invoice    
def get_value():
        # Update process status
        process.config(text='Generating invoice file...')
        
        quantity = get_quantity()
        if quantity:
                # Generate HTML invoice
                generate_html(iphone_cases, notebooks, hidden_cams, quantity)
        else:
                # Generate no charge invoice
                generate_nocharge_html()

# Generate query for inserting item information
def insert_data(data, quantity):
        data_list = []
        for item in data[:int(quantity)]:
                data_list.append(tuple((item[0], item[1])))
        
        return data_list
        
        
# Update database
def update_db():
        quantity = get_quantity()
        #If nothing selected from GUI, return false
        if not quantity:
                return False
        iphone_case_data = iphone_cases
        notebook_data = notebooks
        hidden_cam_data = hidden_cams
        
        # Connect to the database file
        connection = connect(database = "shopping_trolley.db")
        cursor = connection.cursor()

        # Create a table with description and price columns in the database
        cursor.execute("CREATE TABLE IF NOT EXISTS Purchases(Description, Price)")

        # Arrange data to insert
        data = []
        data.extend(insert_data(iphone_case_data, quantity["iphone_case_quantity"]))
        data.extend(insert_data(notebook_data, quantity["notebook_quantity"]))
        data.extend(insert_data(hidden_cam_data, quantity["hidden_cam_quantity"]))
        data_tuple = tuple(data)
        
        # Insert data of each item into the database
        insert_sql = """INSERT INTO Purchases(Description, Price) VALUES (?, ?)"""
        cursor.executemany(insert_sql, data)
        connection.commit()
        cursor.close()
        connection.close()
        
#=================#


# Name of the invoice file. To simplify marking, your program should
# produce its results using this file name.

url_iPhoneCase = 'https://www.zazzle.com/rss?qs=iphone+7+cases'
url_Notebook = 'http://www.tigerdirect.com/xml/rsstigercat17.xml'
url_HiddenCameras = 'https://www.crimezappers.com/rss/catalog/category/cid/97/store_id/1/'

# finding products names, prices, image link
web_page_contents1 = urlopen(url_iPhoneCase).read()
web_page_contents2 = urlopen(url_Notebook).read()
web_page_contents3 = urlopen(url_HiddenCameras).read()


# iPhone Case products
# item names
iPhoneCase_name = findall('<media:title>\s*<!\[CDATA\[\s*([^\]]+)', web_page_contents1)
Pickout_name = iPhoneCase_name[1:6]
# item prices
iPhoneCase_price = findall('<media:price>\$([^<]+)', web_page_contents1)
Pickout_price = iPhoneCase_price[1:6]
Price_currency_exchange = []
for Price_AUD in Pickout_price:
        Price_AUD = float(Price_AUD)*1.35
        Price_AUD = "{0:.2f}".format(Price_AUD)
        Price_currency_exchange.append(Price_AUD) 
# item images
iPhoneCase_image = findall('<media:thumbnail\s* url="([^"]+)', web_page_contents1)
Pickout_image = iPhoneCase_image[1:6]

# bind iphone case item details
iphone_cases = bind_product_details(Pickout_name, Pickout_price, Pickout_image)


# Notebook products
# item names
Notebook_name = findall('<item>\s*<title>\s*([^-]+)', web_page_contents2)
Pickout_name = Notebook_name[1:6]
# item prices
Notebook_price = findall('\$([0-9]*.[0-9]*)',web_page_contents2)
Pickout_price = Notebook_price[1:6]
Price_currency_exchange = []
for Price_AUD in Pickout_price:
        Price_AUD = float(Price_AUD)*1.35
        Price_AUD = "{0:.2f}".format(Price_AUD)
        Price_currency_exchange.append(Price_AUD)
# item images
Notebook_image = findall('<img src=\"([^>]+\s*)\"/><br/>', web_page_contents2)
Pickout_image = Notebook_image[1:6]

# bind iphone case item details
notebooks = bind_product_details(Pickout_name, Pickout_price, Pickout_image)


# Hidden Cameras
# item names
HiddenCameras_name = findall('<item>\s*<title>\s*<!\[CDATA\[\s*([^\]]+)', web_page_contents3)
Pickout_name = HiddenCameras_name[1:6]
# item prices
HiddenCameras_price = findall('<span class=\"price\">\$([^<]+)',web_page_contents3)
Pickout_price = HiddenCameras_price[1:6]
Price_currency_exchange = []
for Price_AUD in Pickout_price:
        Price_AUD = float(Price_AUD)*1.35
        Price_AUD = "{0:.2f}".format(Price_AUD)
        Price_currency_exchange.append(Price_AUD)
# item images
HiddenCameras_image = findall('src=\"([^\"]+)', web_page_contents3)
Pickout_image = HiddenCameras_image[1:6]

# bind iphone case item details
hidden_cams = bind_product_details(Pickout_name, Pickout_price, Pickout_image)

#===========GUI==================

window = Tk()

window.title('Randombox shopping website')

window.configure(background='CadetBlue1')

# GUI label

label_1 = Label(window, text ='Little River Trading Company',
                anchor = 'center', bg = 'CadetBlue1', fg = 'blue', font = ('Arial', 30))


label_2 = Label(window, text = 'Step1. Choose your item Quantities',
                anchor = 'center', bg = 'CadetBlue1',fg = 'red', font = ('Arial', 17) )


label_3 = Label(window, text = 'Step2. Print your invoice when you ready!',
                anchor = 'center', bg = 'CadetBlue1',fg = 'orange', font = ('Arial', 17) )


label_4 = Label(window, text = 'Step3. Watch Overview of your order list!',
                 anchor = 'center',bg = 'CadetBlue1', fg = 'green', font = ('Arial', 17) )

process = Label(window, text = 'Ready...', anchor = 'center', bg = 'CadetBlue1', fg = 'red', font = ('Arial', 17), )

label_5 = Label(window, text = 'Step4. Save your order!',
                 anchor = 'center', bg = 'CadetBlue1',fg = 'purple', font = ('Arial', 17) )

Itemlabel_1 = Label(window, text = 'iPhoneCase',
                    anchor = 'center',bg = 'CadetBlue1', font = ('Arial', 10))
Itemlabel_2 = Label(window, text = 'Notebook',
                    anchor = 'center',bg = 'CadetBlue1', font = ('Arial', 10))
Itemlabel_3 = Label(window, text = 'HiddenCameras',
                    anchor = 'center', bg = 'CadetBlue1',font = ('Arial', 10))


#spinbox

iPhone_spin = Spinbox(window, width = '3', font = ('Arial', 14),
                      from_=0, to=5)
notebook_spin = Spinbox(window, width = '3', font = ('Arial', 14),
                      from_=0, to=5)
hiddenCameras_spin = Spinbox(window, width = '3', font = ('Arial', 14),
                      from_=0, to=5)

# Button

invoice_button = Button(window,text = 'Print invoice', anchor = 'center', font = ('Arial', 14), command=get_value)
saveorder_button = Button(window,text = 'Save Order', anchor = 'center', font = ('Arial', 14), command=update_db)


# Arrange Place


label_1.grid(row = 1,columnspan=2, padx=20, pady=20)
label_2.grid(row = 2,columnspan=2, padx=20, pady=20)
label_3.grid(row = 6,columnspan=2, padx=20, pady=20)
label_4.grid(row = 8,columnspan=2, padx=20, pady=20)
process.grid(row = 9,columnspan=2, padx=20, pady=20)
label_5.grid(row = 10,columnspan=2)
                    
Itemlabel_1.grid(row = 3, column = 0,sticky=E)
Itemlabel_2.grid(row = 4, column = 0,sticky=E)
Itemlabel_3.grid(row = 5, column = 0,sticky=E)
                    
iPhone_spin.grid(row = 3, column = 1,sticky=W)
notebook_spin.grid(row = 4, column = 1,sticky=W)
hiddenCameras_spin.grid(row = 5, column = 1,sticky=W)

invoice_button.grid(row = 7, columnspan=2,)
saveorder_button.grid(row = 11, columnspan=2, padx=20, pady=20)

   
window.mainloop()

#========================
